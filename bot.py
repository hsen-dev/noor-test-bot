from telegram.ext import Updater, CommandHandler, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
import os
import logging


telegram_token = os.environ['TELEGRAM_TOKEN']
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level= logging.INFO)

updater = Updater(telegram_token)
dispatcher = updater.dispatcher

def start(bot, update):
    bot.sendMessage(chat_id=update.message.chat.id, text='Start!')


start_handler = CommandHandler('start', start)

dispatcher.add_handler(start_handler)

if __name__ == '__main__':
    #updater.start_polling()
    updater.start_webhook(listen='0.0.0.0', port=5000, url_path=telegram_token)
    updater.bot.setWebhook(webhook_url='http://bot-test-hsen-bot-test.193b.starter-ca-central-1.openshiftapps.com/'+telegram_token)


